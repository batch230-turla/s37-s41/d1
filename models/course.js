//dependency
const mongooses = require("mongoose");

const courseScheme = new mongoose.Schema(
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	{
	description:{
		type:String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required:[true,"Price is required"]

	},
	isActive:{
		type: Boolean,
	default: true
	},
	createdOn{
		type: Date,
		//the "new Date()" expression instantiates a new "Date" that the current date and time whenever a course is created in our database
		default: new Date(); 
	},
	enrollees:[
	{
		userId:{
			type: String
			required:[true, "UserId is required"]
		},
		{
			enrolledOn:{
				type: Date,
			default: new Date();
			}
		}
	}


		]
}
	
	)

	

/*
"name" : "Information Technology"
"enrollees" : [
{
	"userId" : "001"
	"enrolled" : "Feb 15, 2023"
}
{
	"userId" : "002"
	"enrolled" : "Feb 15, 2023"
}
{
	"userId" : "003"
	"enrolled" : "Feb 15, 2023"
}

	]

//________________________

"_id" : "001",
"name" : "Michael Turla",
"enrolledSubjects" : [
{
	"name" : "Biology"
},
{
	"name" : "Topology"
},
{
	"name" : "Regatory"
},

	]
	*/