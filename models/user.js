const mongooses = require("mongoose");

const userScheme = new mongoose.Schema(
   firstname: {
      type: String,
      required: [true,
         "firstname is required"
      ]
   }, {
      lastname: {
         type: String,
         required: [true,
            "lastname is required"
         ]
      },
      email: {
         type: String,
         required: [true,
            "email is required"
         ]

      },
      password: {
         type: String,
         required: [true,
            "password is required"
         ]

      },
      isAdmin: {
         type: Boolean,
         default: false
      },
      mobile No: {
         type: Number,
         required: [true,
            "Mobile Number is required"
         ]

      },
      enrollments: [{
            courseId: {
               type: String
               required: [true,
                  "courseId is required"
               ]
            },
            {
               enrolledOn: {
                  type: Date,
                  default: new Date();
               }
            },
            Status: {
               type: String
               default: new Enrolled();
            }
         }

      ]

   }

)
module.exports = mongoose.model("User",
   courseSchema);